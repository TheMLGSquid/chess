#include "Bishop.h"

Bishop::Bishop(bool color, const Vector2& position)
    : Piece(color, position, PieceType::Bishop)
{
}

int Bishop::getMaterial() const
{
    return 3;
}

bool Bishop::canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const
{
    const Piece* pieceInPlace = getPiece(newPosition, boardArr);


    if (pieceInPlace != nullptr && m_Color == pieceInPlace->getColor())
    {
        return false; // You cannot capture one of your own pieces.
    }
    else
    {
        int directionX = 1;
        if (m_Pos.x > newPosition.x)
            directionX = -1;
        int diffX = abs(m_Pos.x - newPosition.x);

        int directionY = 1;
        if (m_Pos.y > newPosition.y)
            directionY = -1;
        int diffY = abs(m_Pos.y - newPosition.y);


        if (diffX == diffY)
        {
            bool noPieceInWay = true;
            for (int x = m_Pos.x, y = m_Pos.y; Vector2{x,y} != newPosition && noPieceInWay && x < 8 && y < 8; x += directionX, y += directionY)
            {
                Piece* currentPiece = boardArr[y][x];
                if ((currentPiece != this) && (currentPiece != pieceInPlace)
                    && (currentPiece != nullptr))
                {
                    noPieceInWay = false; // There are pieces in between the bishop and the other piece.
                }
            }
            return noPieceInWay;
        }
        else // A bishop can only go diagonally!
        {
            return false;
        }
    }
}