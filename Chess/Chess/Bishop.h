#pragma once
#include "Piece.h"

// #Bishop
class Bishop : virtual public Piece
{
public:

	Bishop(bool color, const Vector2& position);
	virtual int getMaterial() const override;

	// Inherited via Piece
	virtual bool canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const override;
};

