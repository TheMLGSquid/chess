#include "Board.h"

Board::Board()
	: m_Board({0}), m_WhiteKing(nullptr), m_BlackKing(nullptr), m_CurrentPlayer(WHITE)
{
	
}

Board::~Board()
{
	// Just to remove all references to the pieces on the board.
	m_Board = { 0 };
	m_WhiteKing = nullptr;
	m_BlackKing = nullptr;
}


const std::array<std::array<Piece*, 8>, 8>& Board::getBoard() const
{
	return m_Board;
}

std::array<std::array<Piece*, 8>, 8>& Board::getBoard()
{
	return m_Board;
}
