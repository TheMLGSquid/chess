#pragma once
#include "Piece.h"
#include "King.h"
#include "Knight.h"
#include "Rook.h"
#include "Queen.h"
#include "Pawn.h"
#include "Bishop.h"
// #Board
class Board
{
public:
	Board();
	virtual ~Board();

	// Returns the sub row-array in the board corresponding to the index speicifed
	std::array<Piece*, 8>& operator [](unsigned int index) { return m_Board[index];	} 
	
	// Returns the sub row-array in the board corresponding to the index speicifed
	const std::array<Piece*, 8>& operator [](unsigned int index) const { return m_Board[index]; }
	
	// Returns a reference to the piece in the 2d position entered.
	Piece*& operator[] (const Vector2& position) { return m_Board[position.y][position.x]; }

	// Returns a reference to the piece in the 2d position entered.
	const Piece* const& operator[] (const Vector2& position) const { return m_Board[position.y][position.x]; }
	
	// Returns the reference to the inside board 2d array.
	const std::array<std::array<Piece*, 8>, 8>& getBoard() const;
	
	// Returns the reference to the inside board 2d array.
	std::array<std::array<Piece*, 8>, 8>& getBoard();
	
	// Sets the field that points to the white king on the board to the pointer inserted.
	void setWhiteKing(Piece* king) { m_WhiteKing = king; }

	// Sets the field that points to the black king on the board to the pointer inserted.
	void setBlackKing(Piece* king) { m_BlackKing = king; }


private:
	std::array<std::array<Piece*, 8>, 8> m_Board;
	Piece* m_WhiteKing;
	Piece* m_BlackKing;
	bool m_CurrentPlayer;
};

