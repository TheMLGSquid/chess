#include "Game.h"
#include <iostream>
#define INITIALIZATION_STRING_LENGTH 65
#define QUIT_MESSAGE "quit"

const char Game::normalGame[1024] = "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0";
const char Game::onlyPawnsGame[1024] = "ppppkppppppppppp################################PPPPPPPPPPPPKPPP0";

Game::Game(const std::string& initialStatus, const std::string& firstName, const std::string& secondName)
	: m_Board(), m_White(firstName, WHITE), m_Black(secondName, BLACK), m_CurrentPlayer(WHITE), m_Pipe(), m_Finished(false), m_OutputLog("gameLog.txt")
{
	if (!m_Pipe.connect())
	{
		std::cout << "Couldn't connect to the graphic program!\n";
		m_Finished = true;
	}
	else
	{
		m_Pipe.sendMessageToGraphics((char*)initialStatus.c_str());
		initBoard(initialStatus);
	}
}

Game::Game(const std::string& firstName, const std::string& secondName)
	: m_Board(), m_White(firstName, WHITE), m_Black(secondName, BLACK), m_CurrentPlayer(WHITE), m_Pipe(), m_Finished(false), m_OutputLog("gameLog.txt")
{
	if (!m_Pipe.connect())
	{
		std::cout << "Couldn't connect to the graphic program!\n";
		m_Finished = true;
	}
	else
	{
		m_Pipe.sendMessageToGraphics((char*)Game::normalGame);
		initBoard(Game::normalGame);
	}
}

Game::Game()
	:m_Board(), m_White("FirstPlayer", WHITE), m_Black("SecondPlayer", BLACK), m_CurrentPlayer(WHITE), m_Pipe(), m_Finished(false), m_OutputLog("gameLog.txt")
{
	if (!m_Pipe.connect())
	{
		m_Finished = true;
		std::cout << "Couldn't connect to the graphic program!\n";
	}
	else
	{
		m_Pipe.sendMessageToGraphics((char*)Game::normalGame);
		initBoard(Game::normalGame);
	}
}

void Game::move(const std::string& moveRepr)
{
	Player* currentPlayer = m_WhiteToPlay ? &m_White : &m_Black;
	Player* otherPlayer = m_WhiteToPlay ? &m_Black : &m_White;

	bool promoted = false;
	char message[2] = { MoveCode::Okay, 0 };
	if (moveRepr.size() != 4) // Length must be 4 characters to represent the two squares.
	{
		throw std::exception("Invalid move representation format!\n");
	}

	Vector2 sourcePosition = { moveRepr[0] - 'a', moveRepr[1] - '1' };
	Vector2 destinationPosition = { moveRepr[2] - 'a', moveRepr[3] - '1' };
	Piece* pieceInDestinationPos = m_Board[destinationPosition];
	Piece* pieceToMove = m_Board[sourcePosition];


	if (sourcePosition == destinationPosition)
	{
		message[0] = MoveCode::SameSquare; // Can't move a piece to the square it's already in. 
	}
	else if (m_Board[sourcePosition] == nullptr)
	{
		message[0] = MoveCode::NoPiece; // There isn't a piece to move in the source position.
	}
	else if (m_Board[sourcePosition]->getColor() != m_CurrentPlayer)
	{
		message[0] = MoveCode::NoPiece; // The piece in the source position isn't owned by the player.
	}
	else
	{

		if (pieceInDestinationPos != nullptr && pieceInDestinationPos->getColor() == m_CurrentPlayer)
		{
			message[0] = MoveCode::DestinationIsMine; // Can't capture your own piece.
		}
		else if (!pieceToMove->canMove(destinationPosition, m_Board.getBoard()))
		{
			message[0] = MoveCode::PieceMovement; // The piece cannot move to that location.
		}
		else
		{
			Piece* pieceCaptured = pieceInDestinationPos;
			Vector2 previousPosition = pieceToMove->getPos();
			

			// Checking if the squared captured is an en passant square.
			if (pieceToMove->getType() == Piece::PieceType::Pawn && ((destinationPosition.y + (m_WhiteToPlay ? -1 : 1)) >= 0) && ((destinationPosition.y + (m_WhiteToPlay ? -1 : 1)) < 8))
			{
				if (m_Board[destinationPosition.y + (m_WhiteToPlay ? -1 : 1)][destinationPosition.x] && Piece::PieceType::Pawn == m_Board[destinationPosition.y + (m_WhiteToPlay ? -1 : 1)][destinationPosition.x]->getType())
				{
					Pawn* enPassantablePawn = dynamic_cast<Pawn*>(m_Board[destinationPosition.y + (m_WhiteToPlay ? -1 : 1)][destinationPosition.x]);
					if (enPassantablePawn->getEnPassantable())
					{
						if (enPassantablePawn->getEnPassantSquare() == destinationPosition) // If the en passantable square of the pawn is the destination square
							// The pawn is captured.
						{
							pieceCaptured = enPassantablePawn;
							m_Board[pieceCaptured->getPos()] = nullptr;
							message[0] = MoveCode::EnPassant;
						}
					}
				}
			}

			//in case of castling
			Vector2 newRookPos;
			Vector2 oldRookPos;
			Rook* rookToCastle = nullptr;
			if (pieceToMove->getType() == Piece::PieceType::King && abs(sourcePosition.x - destinationPosition.x) == 2 && abs(sourcePosition.y - destinationPosition.y) == 0)
			{
				if (currentPlayer->getKingStatus(otherPlayer, m_Board.getBoard(), 1) == Player::KingStatus::Safe)
				{
					message[0] = MoveCode::Castle;
					std::unordered_set<Piece*>& otherPieces = otherPlayer->getPieces();
					int castleDir = (sourcePosition.x - destinationPosition.x < 0) ? 1 : -1; //1 is short, -1 is long;
					Vector2 midWayCastling = pieceToMove->getPos() + Vector2{ castleDir, 0 }; //check if castling passes through check
					for (Piece* piece : otherPieces)
					{
						if (piece->canMove(midWayCastling, m_Board.getBoard()))
						{
							message[0] = MoveCode::PieceMovement; // King can't pass through check
						}
					}
					if (message[0] == MoveCode::Castle)
					{
						//change the position of the rook
						rookToCastle = dynamic_cast<Rook*>(m_Board[sourcePosition.y][sourcePosition.x - 0.5 + castleDir * 3.5]);
						oldRookPos = rookToCastle->getPos();
						newRookPos = { (sourcePosition.x + destinationPosition.x) / 2, sourcePosition.y };
						rookToCastle->setPos(newRookPos);
						m_Board[oldRookPos] = nullptr;
						m_Board[newRookPos] = rookToCastle;
						rookToCastle->setMoved();
					}
				}
				else
				{
					message[0] = MoveCode::InvalidCastle;
				}
			}

			m_Board[destinationPosition] = pieceToMove;
			pieceToMove->setPos(destinationPosition);
			
			// Clear the piece moved from it's original position
			m_Board[sourcePosition] = nullptr;
			
			// If a piece was captured, add it to the list of captured pieces and remove it from the 
			// other player's pieces.
			if (pieceCaptured != nullptr)
			{
				currentPlayer->getClaimedPieces().insert(pieceCaptured);
				otherPlayer->getPieces().erase(pieceCaptured);
			}
			
			simulateTurnPassed();
			
			// If the move causes the king to be in check
			if (Player::KingStatus::Safe != currentPlayer->getKingStatus(otherPlayer, m_Board.getBoard(), 1))
			{
				if (rookToCastle && message[0] == MoveCode::Castle)
				{
					//change the position of the rook back
					rookToCastle->setPos(oldRookPos);
					m_Board[newRookPos] = nullptr;
					m_Board[oldRookPos] = rookToCastle;
					rookToCastle->setHasntMoved();
				}

				if (message[0] != MoveCode::InvalidCastle)
				{
					message[0] = MoveCode::DeathWish; // Moving the piece will result in death of the king. Not poggers, big oof.
				}
				// Returning the board and piece lists to their original setup
				m_Board[sourcePosition] = pieceToMove; 
				pieceToMove->setPos(previousPosition);

				if (pieceCaptured != nullptr)
				{
					m_Board[pieceCaptured->getPos()] = pieceCaptured;
					currentPlayer->getClaimedPieces().erase(pieceCaptured);
					otherPlayer->getPieces().insert(pieceCaptured);
				}
				else
				{
					m_Board[destinationPosition] = nullptr;
				}
				simulateTurnRewound();
			}
			else
			{
				m_OutputLog << currentPlayer->getName() << (m_CurrentPlayer ? "(White): " : "(Black): ");
				if (!rookToCastle)
				{
					m_OutputLog << moveRepr[0] << moveRepr[1] << " -> " << moveRepr[2] << moveRepr[3] << '\n';
				}
				else
				{
					m_OutputLog << (destinationPosition.x == 2 ? "O-O-O" : "O-O") << '\n';
				}

				if (pieceToMove->getType() == Piece::PieceType::Pawn && ((pieceToMove->getColor()) ? destinationPosition.y == 7 : destinationPosition.y == 0))
				{
					Piece* promotionPiece = nullptr;
					promoted = true;
					message[0] = MoveCode::Promotion;
					m_Pipe.sendMessageToGraphics(message);
					std::string newPieceType = m_Pipe.getMessageFromGraphics();

					switch (newPieceType[0])
					{
					case 'Q':
						promotionPiece = new Queen(pieceToMove->getColor(), pieceToMove->getPos());
						break;

					case 'B':
						promotionPiece = new Bishop(pieceToMove->getColor(), pieceToMove->getPos());
						break;

					case 'K':
						promotionPiece = new Knight(pieceToMove->getColor(), pieceToMove->getPos());
						break;

					case 'R':
						promotionPiece = new Rook(pieceToMove->getColor(), pieceToMove->getPos());
						break;

					default:
						throw std::exception("Invalid piece type to promote to");
						break;
					}
					
					promotionPiece->setMoved();
					currentPlayer->getPieces().erase(pieceToMove);
					currentPlayer->getPieces().insert(promotionPiece);
					m_Board[pieceToMove->getPos()] = promotionPiece;
					delete pieceToMove;
					message[0] = MoveCode::Okay;
				}

				// Checking the other player's king's status after the current move.
				switch(otherPlayer->getKingStatus(currentPlayer, m_Board.getBoard()))
				{	
				case Player::KingStatus::Checked:
					message[0] += 1; // Checked the other player!
					break;
				
				case Player::KingStatus::Mated:
					message[0] += 2; // The other player has no moves that can save his king's life.
					std::cout << otherPlayer->getName() << " was mated by " << currentPlayer->getName() << '\n';
					m_OutputLog << currentPlayer->getName() << (m_CurrentPlayer ? "(White)" : "(Black)") << " has won the game!"; 
					m_Finished = true;
					break;
					
				case Player::KingStatus::Safe:
					message[0] += 0; // Normal move.
					break;
				}
				if (!promoted) 
				{ 
					pieceToMove->setMoved(); 
				}

				std::cout << currentPlayer->getName() << "(" << currentPlayer->getColor() << ") is ";
				int materialDiff = (otherPlayer->getMaterial() - currentPlayer->getMaterial());
				if (materialDiff >= 0)
					std::cout << '+';
				std::cout << materialDiff << " in material" << '\n';

				m_CurrentPlayer = !m_CurrentPlayer; // The turn has successfully ended, switching players.			
			}
		}

	}	
	m_Pipe.sendMessageToGraphics(message);
	
}

void Game::move()
{
	std::string moveRepr = m_Pipe.getMessageFromGraphics();

	if (moveRepr != QUIT_MESSAGE)
	{
		move(moveRepr);
	}
	else
	{
		m_Finished = true;
	}
}

void Game::initBoard(const std::string& initialStatus)
{
	int stringIndex = 0;

	if (initialStatus.size() != INITIALIZATION_STRING_LENGTH) // status for each square and the starting player
	{
		throw std::exception("Incorrect game initialization status format!\n");
	}
	
	m_CurrentPlayer = !(initialStatus[64] - '0');

	for (int i = 7; i >= 0; i--)
	{
		for (int j = 0; j < 8; j++)
		{
			switch (initialStatus[stringIndex])
			{
			// Black pieces
			case 'k':
				m_Board[i][j] = new King(BLACK, { j,i });
				m_Board.setBlackKing(m_Board[i][j]);
				m_Black.setKing(m_Board[i][j]);		
				break;

			case 'q':
				m_Board[i][j] = new Queen(BLACK, { j,i });
				break;
			
			case 'r':
				m_Board[i][j] = new Rook(BLACK, { j,i });
				break;
			
			case 'n':
				m_Board[i][j] = new Knight(BLACK, { j,i });
				break;
			
			case 'b':
				m_Board[i][j] = new Bishop(BLACK, { j,i });
				break;
			
			case 'p':
				m_Board[i][j] = new Pawn(BLACK, { j,i });
				break;

			// White pieces
			case 'K':
				m_Board[i][j] = new King(WHITE, { j,i });
				m_Board.setWhiteKing(m_Board[i][j]);
				m_White.setKing(m_Board[i][j]);
				break;
			case 'Q':
				m_Board[i][j] = new Queen(WHITE, { j,i });
				break;
			case 'R':
				m_Board[i][j] = new Rook(WHITE, { j,i });
				break;
			case 'N':
				m_Board[i][j] = new Knight(WHITE, { j,i });
				break;
			case 'B':
				m_Board[i][j] = new Bishop(WHITE, { j,i });
				break;
			case 'P':
				m_Board[i][j] = new Pawn(WHITE, { j,i });
				break;

			// empty square
			case '#':
				m_Board[i][j] = nullptr;
				break;

			// Incorrect piece format.
			default:
				throw std::exception("Incorrect piece format");
				break;
			}

			if (m_Board[i][j] != nullptr)
			{
				if (m_Board[i][j]->getColor() == WHITE)
				{
					m_White.getPieces().insert(m_Board[i][j]);
				}
				else
				{
					m_Black.getPieces().insert(m_Board[i][j]);
				}
			}
			stringIndex++;
		}
	}
	
}

void Game::simulateTurnPassed()
{
	for (Piece* piece : m_White.getPieces())
	{
		piece->turnPassed();
	}
	for (Piece* piece : m_Black.getPieces())
	{
		piece->turnPassed();
	}
}

void Game::simulateTurnRewound()
{
	for (Piece* piece : m_White.getPieces())
	{
		piece->turnRewound();
	}
	for (Piece* piece : m_Black.getPieces())
	{
		piece->turnRewound();
	}
}




Game::~Game()
{

	// Deleting every piece from memory.
	for (Piece* piece : m_White.getPieces())
	{
		delete piece;
	}
	for (Piece* piece : m_White.getClaimedPieces())
	{
		delete piece;
	}
	for (Piece* piece : m_Black.getPieces())
	{
		delete piece;
	}
	for (Piece* piece : m_Black.getClaimedPieces())
	{
		delete piece;
	}

	// Closing all connections regarding the program.
	m_OutputLog.close();
	m_Pipe.close();
}

