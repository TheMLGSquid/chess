#pragma once


#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"

#include "Pawn.h"
#include "Board.h"
#include "Player.h"
#include "Pipe/Pipe.h"
#include <fstream>

// #Game
class Game
{
public:
	static const char normalGame[1024];
	static const char onlyPawnsGame[1024];//for fun

	// Initialization the game with the string representing the game status specified and with the names entered 
	// for the players.
	Game(const std::string& initialStatus, const std::string& firstName, const std::string& secondName);

	// Initializes the game with the default game setup and the two players with the names specified.
	Game(const std::string& firstName, const std::string& secondName);
	
	// Initializes the game with default names and the default game setup.
	Game();

	// Deletes and frees all of the pointers to the pieces in the board and the pieces captured.
	virtual ~Game();
	
	// The code to send to the graphic program to tell it how the move went.
	enum MoveCode { Okay = '0', Check, MadeChess, NoPiece, DestinationIsMine, DeathWish, OutsideBoundries, PieceMovement, SameSquare, EnPassant, EnPassantCheck, EnPassantMate, InvalidCastle, Castle, CastleCheck, CastleMate, Promotion	};
	
	// Moves the piece from the source position represented in the 2 first characters and
	// the destination in the 2 characters following up the first two.
	void move(const std::string& moveRepr);	

	// Moves the piece from the source to the destination both sent by the graphics program.
	void move();	

	// Initializes the board with the initialization string following the format of
	// the first 64 characters repesenting each piece on the board, starting from top left, and the last character being 0 or 1 for 
	// what the first player is where 1 is black and 0 is white.
	void initBoard(const std::string& initialStatus);

	// Return whether or not the game has been finished yet.
	bool running() const { return !m_Finished; }

	void simulateTurnPassed();
	void simulateTurnRewound();

	// Just for debugging.
	friend void printBoard(const Game&);
private:
	Board m_Board;
	Player m_White;
	Player m_Black;
	union { bool m_WhiteToPlay; bool m_CurrentPlayer; };
	Pipe m_Pipe;
	bool m_Finished;
	std::ofstream m_OutputLog;
};


