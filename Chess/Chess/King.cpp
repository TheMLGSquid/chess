#include "King.h"
#include "Rook.h"

King::King(bool color, const Vector2& position)
    : Piece(color, position, PieceType::King)
{
}

int King::getMaterial() const
{
    return 0;//priceless so doesn't affect the calculation
}

bool King::canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const
{
    const Piece* pieceInPlace = getPiece(newPosition, boardArr);
    Vector2 distance = m_Pos - newPosition;

    if (pieceInPlace != nullptr && m_Color == pieceInPlace->getColor())
    {
        return false; // You cannot capture one of your own pieces.
    }
        if (abs(distance.x) == 2 && distance.y == 0)
    {
        int castleDir = (distance.x < 0) ? 1 : -1;//1 is short, -1 is long;
        int row = m_IsWhite ? 0 : 7;
        if (((m_Pos.x - 0.5 + castleDir * 3.5) < 8) && ((m_Pos.x - 0.5 + castleDir * 3.5) >= 0))
        {

            Piece* rookToCastle = dynamic_cast<Rook*>(boardArr[row][m_Pos.x - 0.5 + castleDir * 3.5]);
            if (!hasMoved())//king hasn't moved
            {
                if (rookToCastle && !rookToCastle->hasMoved())//theres a rook and it hasn't moved
                {
                    if (rookToCastle->canMove(Vector2{ m_Pos.x + castleDir, m_Pos.y }, boardArr) && !boardArr[row][long(m_Pos.x) + castleDir])//if the way to castle isn't obscured
                    {
                        return true;
                    }
                }
            }
        }
        else
        {
            return false;
        }
    }
    return (abs(distance.x) <= 1) && (abs(distance.y) <= 1);//regular king move
}
