#pragma once
#include "Piece.h"
// #King
class King : virtual public Piece
{
public:
	King(bool color, const Vector2& position);
	virtual int getMaterial() const override;

	// Inherited via Piece
	virtual bool canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const override;
};

