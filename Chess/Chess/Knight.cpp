#include "Knight.h"

Knight::Knight(bool color, const Vector2& position)
    : Piece(color, position, PieceType::Knight)
{   
}

int Knight::getMaterial() const
{
    return 3;
}

bool Knight::canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const
{
    const Piece* pieceInPlace = getPiece(newPosition, boardArr);
    Vector2 distances = { abs(m_Pos.x - newPosition.x), abs(m_Pos.y - newPosition.y) };


    if (pieceInPlace != nullptr && m_Color == pieceInPlace->getColor())
    {
        return false; // You cannot capture one of your own pieces.
    }
    else
    {
        return (distances.x == 1 && distances.y == 2) || (distances.x == 2 && distances.y == 1);
    }
}
