#pragma once
#include "Piece.h"

// #Knight
class Knight : virtual public Piece
{
public:

	Knight(bool color, const Vector2& position);
	virtual int getMaterial() const override;

	// Inherited via Piece
	virtual bool canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const override;
};

