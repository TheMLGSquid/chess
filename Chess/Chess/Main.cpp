#include <iostream>
#include "Game.h"
#include <chrono>
#include <thread>
#include <SFML/Network.hpp>

void printBoard(const Game& game)
{
	for (int i = 7; i >= 0; i--)
	{
		for (int j = 0; j < 8; j++)
		{
			char character = '-';
			if (game.m_Board.getBoard()[i][j])
			{	
				switch (game.m_Board.getBoard()[i][j]->getType())
				{
				case Piece::PieceType::King:
					character = 'K';
					break;
				case Piece::PieceType::Queen:
					character = 'Q';
					break;
				case Piece::PieceType::Pawn:
					character = 'P';
					break;
				case Piece::PieceType::Rook:
					character = 'R';
					break;
				case Piece::PieceType::Bishop:
					character = 'B';
					break;
				case Piece::PieceType::Knight:
					character = 'N';
					break;
				default:
					character = '#';
					break;
				}
				if (game.m_Board.getBoard()[i][j]->getColor())
				{
					character -= ('A' - 'a');//switch to lowercase if piece is white
				}
			}
			std::cout << character << ' ';
		}
		std::cout << '\n';
	}

}


int main()
{
	Game g("Tal", "Noam");
	while (g.running())
	{
		g.move();
		printBoard(g);
	}
}