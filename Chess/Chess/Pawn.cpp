#include "Pawn.h"
#include <memory>

Pawn::Pawn(bool color, const Vector2& position)
	: Piece(color, position, PieceType::Pawn), m_EnPassantable(false), m_EnPassantSquare({ -1, -1 }), m_OriginalPosition(position), m_PreviousEnPassantStatus(false)
{
	
}

void Pawn::setMoved()
{
	m_PreviousEnPassantStatus = m_EnPassantable;
	if (m_EnPassantable)
	{
		m_EnPassantable = false;
	}
	else if (abs((m_OriginalPosition - m_Pos).y) == 2)
	{
		m_EnPassantable = true;
		m_EnPassantSquare = { m_Pos.x, (m_OriginalPosition.y + m_Pos.y) / 2};
	}
	Piece::setMoved();
}

int Pawn::getMaterial() const
{
	return 1;
}

bool Pawn::canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const
{
	const Piece* pieceInPlace = getPiece(newPosition, boardArr);
	Vector2 distance = newPosition - m_Pos;
	if (pieceInPlace != nullptr)
	{
		if (m_Color == pieceInPlace->getColor())
		{
			return false; // You cannot capture one of your own pieces.
		}
		else
		{
			return abs(distance.x) == 1 && (
				    (m_IsWhite && distance.y == 1)
					|| (!m_IsWhite && distance.y == -1)
				);
		}
	}
	else
	{
		if (!m_HasMoved)
		{
			if (abs(distance.y) == 2)
			{
				if (boardArr[(m_Pos.y + newPosition.y) / 2][m_Pos.x] != nullptr)
				{
					return false;
				}
			}
			return distance.x == 0 && 
				(m_IsWhite ? (distance.y == 1 || distance.y == 2) : (distance.y == -1 || distance.y == -2));
		}
		else
		{
			if((newPosition.y + (m_Color ? -1 : 1)) >= 0 && (newPosition.y + (m_Color ? -1 : 1) < 8))
			{ 
				if (boardArr[newPosition.y + (m_Color ? -1 : 1)][newPosition.x])
				{
					if (boardArr[newPosition.y + (m_Color ? -1 : 1)][newPosition.x]->getType() == Piece::PieceType::Pawn)
					{

						Pawn* pawnPointer = dynamic_cast<Pawn*>(boardArr[newPosition.y + (m_Color ? -1 : 1)][newPosition.x]);
						if (pawnPointer->m_EnPassantable && pawnPointer->m_EnPassantSquare == newPosition)
						{
							return abs(distance.x) == 1 && (
								(m_IsWhite && distance.y == 1)
								|| (!m_IsWhite && distance.y == -1)
								);
						}
					}
				}
			}
			return distance.x == 0 && 
				(m_IsWhite ? (distance.y == 1) : (distance.y == -1));
		}
	}
}
