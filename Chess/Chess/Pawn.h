#pragma once
#include "Piece.h"

// #Pawn
class Pawn : virtual public Piece
{
public:

	Pawn(bool color, const Vector2& position);
	void virtual setMoved() override;
	virtual int getMaterial() const override;

	Vector2 getEnPassantSquare() const { return m_EnPassantSquare; }
	bool getEnPassantable() const { return m_EnPassantable; }
	virtual void turnPassed() override { if (m_EnPassantable) { m_PreviousEnPassantStatus = m_EnPassantable; m_EnPassantable = false; } }
	virtual void turnRewound() override {m_EnPassantable = m_PreviousEnPassantStatus; }
	// Inherited via Piece
	virtual bool canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const override;
protected:
	bool m_EnPassantable;
	Vector2 m_EnPassantSquare;
	Vector2 m_OriginalPosition;
	bool m_PreviousEnPassantStatus;
};

