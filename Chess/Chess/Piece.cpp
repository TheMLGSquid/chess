#include "Piece.h"

Piece::Piece(bool color, const Vector2& pos, Piece::PieceType type)
    : m_Color(color), m_Pos(pos), m_Type(type), m_HasMoved(false)
{

}

Piece::Piece(bool color, Piece::PieceType type)
    : m_Color(color), m_Pos{-1,-1}, m_Type(type), m_HasMoved(false)
{

}

void Piece::turnPassed()
{
}

void Piece::setPos(const Vector2& position)
{
    m_Pos = position;
}

void Piece::turnRewound()
{

}


const Piece* const Piece::getPiece(const Vector2& newPos, const std::array<std::array<Piece*, 8>, 8>& boardArr) const
{
    if (newPos.x < 0 || newPos.x >= 8 || newPos.y < 0 || newPos.y >= 8)
    {
        throw MoveException(newPos); // Move is outside boundries which isn't supposed to happen
    }
    
    return boardArr[newPos.y][newPos.x];
}

bool Piece::getColor() const
{
    return m_Color;
}

Vector2 Piece::getPos() const
{
    return m_Pos;
}

Piece::PieceType Piece::getType() const
{
    return m_Type;
}

bool Piece::hasMoved() const
{
    return m_HasMoved;
}

void Piece::setMoved()
{
    m_HasMoved = true;
}

MoveException::MoveException(const Vector2& pos)
    : std::exception(), m_WhatsWrong()
{
    std::ostringstream stream;
    stream << "Can't move to " << pos << '\n';
    m_WhatsWrong = stream.str();
}

const char* MoveException::what() const
{
    return m_WhatsWrong.c_str();
}

MoveException::~MoveException()
{
}
