#pragma once
#define WHITE (true)
#define BLACK (false)

#include "Vector2.h"
#include <string>
#include <array>
#include <exception>
#include <sstream>

#define max(x,y) (x < y) ? y : x
#define min(x,y) (x > y) ? y : x

// #Piece

class MoveException : std::exception
{
public:
	MoveException(const Vector2& pos);
	virtual const char * what() const override;
	virtual ~MoveException();
protected:
	std::string m_WhatsWrong;
};

class Piece
{
public:
	enum class PieceType {Pawn, Rook, Knight, Bishop, King, Queen};

	virtual ~Piece() {};
	Piece(bool color, const Vector2& pos, PieceType type);
	Piece(bool color, PieceType type);
	virtual bool canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const = 0;
	
	const Piece* const getPiece(const Vector2& newPos, const std::array<std::array<Piece*, 8>, 8>& boardArr) const;
	virtual int getMaterial() const = 0;
	bool getColor() const;
	Vector2 getPos() const;
	PieceType getType() const;
	bool hasMoved() const;
	virtual void setMoved();
	virtual void turnPassed();
	virtual void turnRewound();
	void setPos(const Vector2& position);


protected:
	union { bool m_Color; bool m_IsWhite; };
	Vector2 m_Pos;
	PieceType m_Type;
	bool m_HasMoved;
};

