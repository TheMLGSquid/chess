#include "Player.h"
#include <iostream>
const unsigned int Player::s_MaxMovesUntilMate = 2;

Player::Player(const std::string& name, bool color)
    : m_Name(name), m_PiecesCaptured(), m_AlivePieces(), m_Color(color), m_King(nullptr)
{
}

Player::~Player()
{
    m_King = nullptr;
    m_AlivePieces.clear();
    m_PiecesCaptured.clear();
}



std::unordered_set<Piece*>& Player::getPieces()
{
    return m_AlivePieces;
}

std::unordered_set<Piece*>& Player::getClaimedPieces()
{
    return m_PiecesCaptured;
}

const std::unordered_set<Piece*>& Player::getPieces() const
{
    return m_AlivePieces;
}

const std::unordered_set<Piece*>& Player::getClaimedPieces() const
{
    return m_PiecesCaptured;
}

Player::KingStatus Player::getKingStatus(Player* other, std::array<std::array<Piece*, 8>, 8>& boardArr, unsigned int callAmount) 
{
    KingStatus kingStatus = KingStatus::Safe;
    std::unordered_set<Piece*>& otherPieces = other->getPieces();
    bool gameOver = false;
    bool foundWayOut = false;

    for (Piece* piece : otherPieces)
    {
        if (piece->canMove(m_King->getPos(), boardArr))
        {
            kingStatus = KingStatus::Checked; // If the other piece can move to the location of the king, the king is chooked.
        }
    }
    

    if (kingStatus == KingStatus::Checked && callAmount >= Player::s_MaxMovesUntilMate)
    {
        gameOver = true; // If 2 moves into the future from the move the king is still in check, it's a mate.
        kingStatus = KingStatus::Mated;
    }
    else if (kingStatus == KingStatus::Checked)
    {
        kingStatus = KingStatus::Mated;

        if(!gameOver)
        { 
            for (Piece* piece : m_AlivePieces)
            {
                for (int i = 0; i < 8 && !gameOver && !foundWayOut; i++)
                {
                    for (int j = 0; j < 8 && !gameOver && !foundWayOut; j++)
                    {
                        if (piece->canMove({ j, i }, boardArr))
                        {
                            Vector2 previousPos = piece->getPos();
                            Piece* claimedPiece = boardArr[i][j];

                            // "Go to the next turn" 
                            boardArr[i][j] = piece;
                            boardArr[previousPos.y][previousPos.x] = nullptr;
                            piece->setPos({ j,i });
                            if (claimedPiece)
                            {
                                otherPieces.erase(claimedPiece);
                                m_PiecesCaptured.insert(claimedPiece);
                            }
                            
                            if (KingStatus::Safe == getKingStatus(other, boardArr, callAmount + 1))
                            {
                                kingStatus = KingStatus::Checked; 
                                foundWayOut = true;
                            }

                            // Undo the changes
                            boardArr[i][j] = claimedPiece;
                            boardArr[previousPos.y][previousPos.x] = piece;
                            piece->setPos(previousPos);
                            if (claimedPiece)
                            {
                                otherPieces.insert(claimedPiece);
                                m_PiecesCaptured.erase(claimedPiece);
                            }

                        }
                    }
                }
            }
        }
    }
    return kingStatus;
}

int Player::getMaterial() const
{
    int material = 0;
    for (Piece* piece : m_AlivePieces)
    {
        material += piece->getMaterial();
    }
    return material;
}

std::string Player::getColor() const
{
    if (m_Color)
        return "White";
    return "Black";
}