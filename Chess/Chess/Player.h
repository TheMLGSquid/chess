#pragma once

#include "Piece.h"
#include "King.h"
#include <vector>
#include <unordered_set>
// #Player
class Player
{
public:
	static const unsigned int s_MaxMovesUntilMate;
	
	enum class KingStatus { Safe, Checked, Mated };
	Player(const std::string& name, bool color);
	virtual ~Player();
	
	std::unordered_set<Piece*>& getPieces();
	std::unordered_set<Piece*>& getClaimedPieces();
	const std::unordered_set<Piece*>& getPieces() const;
	const std::unordered_set<Piece*>& getClaimedPieces() const;
	void setKing(Piece* king) {m_King = king; }
	KingStatus getKingStatus(Player* other, std::array<std::array<Piece*, 8>, 8>& boardArr, unsigned int callAmount = 0);
	std::string getName() const { return m_Name; }
	int getMaterial() const;
	std::string getColor() const;
private:
	std::string m_Name;
	std::unordered_set<Piece*> m_PiecesCaptured;
	std::unordered_set<Piece*> m_AlivePieces;
	union {	bool m_Color; bool m_IsWhite; };
	Piece* m_King;
};

