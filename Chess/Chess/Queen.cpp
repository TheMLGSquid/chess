#include "Queen.h"

Queen::Queen(bool color, const Vector2& position)
    : Piece(color, position, PieceType::Queen), Rook(color, position), Bishop(color, position)
{
    
}

int Queen::getMaterial() const
{
    return 9;
}

bool Queen::canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const
{
    return Rook::canMove(newPosition, boardArr) || Bishop::canMove(newPosition, boardArr);
}
