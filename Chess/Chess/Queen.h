#pragma once
#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"


// #Queen
class Queen : virtual public Rook, virtual public Bishop
{
public:
	Queen(bool color, const Vector2& position);
	virtual int getMaterial() const override;
	virtual bool canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const override;
};

