#include "Rook.h"

Rook::Rook(bool color, const Vector2& position)
    : Piece(color, position, PieceType::Rook)
{
    
}

int Rook::getMaterial() const
{
    return 5;
}

bool Rook::canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const
{
    const Piece* pieceInPlace = getPiece(newPosition, boardArr);
    
    
    if (pieceInPlace != nullptr && m_Color == pieceInPlace->getColor())
    {
        return false; // You cannot capture one of your own pieces.
    }
    else
    {
        int directionX = 1;
        if (m_Pos.x > newPosition.x)
            directionX = -1;
        
        int directionY = 1;
        if (m_Pos.y > newPosition.y)
            directionY = -1;
            
            
        if (m_Pos.y == newPosition.y)
        {
            bool noPieceInWay = true;
            for (int i = m_Pos.x; i != newPosition.x && noPieceInWay && i < 8 && i >= 0; i += directionX)
            {
                Piece* currentPiece = boardArr[m_Pos.y][i];
                if ((currentPiece != this) && (currentPiece != pieceInPlace)
                                && (currentPiece != nullptr))
                {
                    noPieceInWay = false; // There are pieces in between the rook and the other piece.
                }
            }
            return noPieceInWay;
        } 
        else if (m_Pos.x == newPosition.x)
        {
            bool noPieceInWay = true;
            for (int i = m_Pos.y; i != newPosition.y && noPieceInWay && i < 8 && i >= 0; i += directionY)
            {
                Piece* currentPiece = boardArr[i][m_Pos.x];
                if ((currentPiece != this) && (currentPiece != pieceInPlace)
                    && (currentPiece != nullptr))
                {
                    noPieceInWay = false; // There are pieces in between the rook and the other piece.
                }
            }
            return noPieceInWay;
        }
        else // A rook cannot go diagonally!
        {
            return false;
        }           
    }
}