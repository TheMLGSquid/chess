#pragma once
#include "Piece.h"

// #Rook
class Rook : virtual public Piece
{
public:
	Rook(bool color, const Vector2& position);
	void setHasntMoved() { m_HasMoved = false;}
	virtual int getMaterial() const override;

	// Inherited via Piece
	virtual bool canMove(const Vector2& newPosition, const std::array<std::array<Piece*, 8>, 8>& boardArr) const override;
};

