#pragma once
#include <SFML/Network.hpp>
#include <iostream>
#include <tuple>


namespace Sockets
{

	sf::TcpSocket::Status sendMessage(sf::TcpSocket& socket, const std::string& message)
	{
		char* data = (char*)message.c_str();
		size_t size = message.size();

		return socket.send(data, size);
	}
		
	std::tuple<std::string, sf::TcpSocket::Status> receiveMessage(sf::TcpSocket& socket, size_t size = 1024)
	{
		char* data = new char[size + 1];
		size_t sizeRecieved;

		sf::TcpSocket::Status status = socket.receive(data, size, sizeRecieved);
		data[sizeRecieved] = NULL;
		
		return { std::string(data), status };
	}
}