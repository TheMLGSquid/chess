#pragma once
#include <ostream>

#include <math.h>
struct Vector2
{
	int x, y;


	Vector2 operator+(const Vector2& other) const
	{
		return { x + other.x, y + other.y };
	}
	Vector2 operator-(const Vector2& other) const
	{
		return { x - other.x, y - other.y };
	}

	inline bool operator ==(const Vector2& other) const
	{
		return x == other.x && y == other.y;
	}

	inline bool operator !=(const Vector2& other) const
	{
		return !(*this == other);
	}

	inline Vector2 absolute() const
	{
		return { abs(x), abs(y) };
	}

	inline Vector2 distance(const Vector2& other) const
	{
		return (*this - other).absolute();
	}

	inline bool operator < (const Vector2& other) const
	{
		return x < other.x && y < other.y;
	}

	inline bool operator > (const Vector2& other) const
	{
		return x > other.x && y > other.y;
	}

	inline bool operator <= (const Vector2& other) const
	{
		return *this < other || *this == other;
	}

	inline bool operator >= (const Vector2& other) const
	{
		return *this > other || *this == other;
	}

	friend std::ostream& operator << (std::ostream& stream, const Vector2& vec)
	{
		return stream << "{" << vec.x << ", " << vec.y << "}";
	}
};
