﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Media;

namespace chessGraphics
{
    public partial class Form1 : Form
    {
        private string directoryBeginning;
        private double player1Time = 300;
        private double player2Time = 300;
        private int increment = 0;
        WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();
        private bool promoteNow = false;
        private bool startGame = false;
        private bool waitForButton = false;
        private Square srcSquare;
        private Square dstSquare;

        private pipe enginePipe;
        private Button[,] matBoard;

        private bool isCurPlWhite = true;
        private bool isGameOver = false;
        private Label[] labels = new Label[32];

        private const int BOARD_SIZE = 8;

        public Form1()
        {
            InitializeComponent();
        }

        private void initForm()
        {

            int length = Application.ExecutablePath.Split('\\').Length - 2;
            string[] newPathParts = new string[length];
            Array.Copy(Application.ExecutablePath.Split('\\'), newPathParts, length);
            this.directoryBeginning = System.IO.Path.GetDirectoryName(string.Join("\\", newPathParts));
            enginePipe.connect();

            Invoke((MethodInvoker)delegate
            {
                lblCurrentPlayer.Visible = true;
                label1.Visible = true;

                string s = enginePipe.getEngineMessage();

                if (s.Length != (BOARD_SIZE * BOARD_SIZE + 1))
                {
                    MessageBox.Show("The length of the board's string is not according the PROTOCOL");
                    this.Close();
                }
                else
                {
                    isCurPlWhite = (s[s.Length - 1] == '0');
                    textBox1.Text = string.Format($"{(player1Time / 60):00}:{(player1Time % 60):00}");
                    textBox2.Text = string.Format($"{(player2Time / 60):00}:{(player2Time % 60):00}");
                    timer1.Stop();
                    timer2.Stop();
                    textBox1.Show();
                    textBox2.Show();
                    setupGameBoard();
                    paintBoard(s);
                }
            });
        }

        private Thread connectionThread;



        private void startScreen()
        {
            this.labels[0] = label3;
            this.labels[1] = label4;
            this.labels[2] = label5;
            this.labels[3] = label6;
            this.labels[4] = label7;
            this.labels[5] = label8;
            this.labels[6] = label9;
            this.labels[7] = label10;
            this.labels[8] = label11;
            this.labels[9] = label12;
            this.labels[10] = label13;
            this.labels[11] = label14;
            this.labels[12] = label15;
            this.labels[13] = label16;
            this.labels[14] = label17;
            this.labels[15] = label18;
            this.labels[16] = label19;
            this.labels[17] = label20;
            this.labels[18] = label21;
            this.labels[19] = label22;
            this.labels[20] = label23;
            this.labels[21] = label24;
            this.labels[22] = label25;
            this.labels[23] = label26;
            this.labels[24] = label27;
            this.labels[25] = label28;
            this.labels[26] = label29;
            this.labels[27] = label30;
            this.labels[28] = label31;
            this.labels[29] = label32;
            this.labels[30] = label33;
            this.labels[31] = label34;

            foreach (Label label in labels)
            {
                label.Visible = false;
            }
            this.textBox1.Visible = false;
            this.textBox2.Visible = false;
            this.textBox1.Enabled = false;
            this.textBox2.Enabled = false;


            this.Show();
        }

        private void setupGameBoard()
        {
            this.labels[0] = label3;
            this.labels[1] = label4;
            this.labels[2] = label5;
            this.labels[3] = label6;
            this.labels[4] = label7;
            this.labels[5] = label8;
            this.labels[6] = label9;
            this.labels[7] = label10;
            this.labels[8] = label11;
            this.labels[9] = label12;
            this.labels[10] = label13;
            this.labels[11] = label14;
            this.labels[12] = label15;
            this.labels[13] = label16;
            this.labels[14] = label17;
            this.labels[15] = label18;
            this.labels[16] = label19;
            this.labels[17] = label20;
            this.labels[18] = label21;
            this.labels[19] = label22;
            this.labels[20] = label23;
            this.labels[21] = label24;
            this.labels[22] = label25;
            this.labels[23] = label26;
            this.labels[24] = label27;
            this.labels[25] = label28;
            this.labels[26] = label29;
            this.labels[27] = label30;
            this.labels[28] = label31;
            this.labels[29] = label32;
            this.labels[30] = label33;
            this.labels[31] = label34;

            foreach (Label label in labels)
            {
                label.Visible = true;
            }
            this.textBox1.Visible = true;
            this.textBox2.Visible = true;

            this.Show();
        }

        
        

        private void Form1_Load(object sender, EventArgs e)
        {
            startScreen();
            chooseTime();
            enginePipe = new pipe();
        
            //MessageBox.Show("Press OK to start waiting for engine to connect...");
            connectionThread = new Thread(initForm);
            connectionThread.Start();
            connectionThread.IsBackground = true;
            InitiatePromotion.Click += InitiatePromotion_Click;
            InitiateGame.Click += InitiateGame_Click;

            //initForm();
        }

        private void chooseTime()
        {
            InitiateGame.Visible = true;
            IncrementChoice.Visible = true;
            TimeChoice.Visible = true;
            TimeChoice.SelectedItem = "05 Minutes - Blitz";
            IncrementChoice.SelectedItem = "No Increment";
            this.Refresh();

            while (!this.startGame)
            {
                Application.DoEvents();
            }

            int playerTime = int.Parse(TimeChoice.SelectedItem.ToString().Substring(0, 2));
            if (IncrementChoice.SelectedItem.ToString()[0] == '-')
            {
                increment = -3;
            }
            else if (IncrementChoice.SelectedItem.ToString()[0] == 'N')
            {
                increment = 0;
            }
            else
            {
                int.Parse(IncrementChoice.SelectedItem.ToString()[0].ToString());
            }

            player1Time = playerTime * 60;
            player2Time = playerTime * 60;
            InitiateGame.Visible = false;
            TimeChoice.Visible = false;
            IncrementChoice.Visible = false;
            this.Refresh();
        }

        private Image getImageBySign(char sign)
        {
            switch (sign)
            {
                case 'q':
                    return Properties.Resources.q_black;

                case 'Q':
                    return Properties.Resources.q_white;

                case 'k':
                    return Properties.Resources.k_black;

                case 'K':
                    return Properties.Resources.k_white;

                case 'p':
                    return Properties.Resources.p_black;

                case 'P':
                    return Properties.Resources.p_white;

                case 'r':
                    return Properties.Resources.r_black;

                case 'R':
                    return Properties.Resources.r_white;

                case 'n':
                    return Properties.Resources.n_black;

                case 'N':
                    return Properties.Resources.n_white;

                case 'b':
                    return Properties.Resources.b_black;

                case 'B':
                    return Properties.Resources.b_white;

                case '#':
                    return null;

                default:
                    return Properties.Resources.x;
            }
        }

        private void paintBoard(string board)
        {
            int i, j, z = 0;

            matBoard = new Button[BOARD_SIZE, BOARD_SIZE];

            btnBoard.FlatAppearance.MouseOverBackColor = Color.LightGray;

            Button newBtn;
            Point pnt;

            int currentWidth = btnBoard.Location.X;
            int currentHeight = btnBoard.Location.Y;

            bool isColBlack = false;
            bool isRowBlack = false;

            this.SuspendLayout();

            lblCurrentPlayer.Text = isCurPlWhite ? "White" : "Black";

            for (i = 0; i < BOARD_SIZE; i++)
            {
                currentWidth = btnBoard.Location.X;
                isColBlack = isRowBlack;

                for (j = 0; j < BOARD_SIZE; j++)
                {
                    newBtn = new Button();
                    matBoard[i, j] = newBtn;

                    newBtn.FlatAppearance.MouseOverBackColor = btnBoard.FlatAppearance.MouseOverBackColor;
                    
                    newBtn.BackColor = isColBlack ? Color.Gray : Color.White;
                    newBtn.FlatAppearance.BorderColor = btnBoard.FlatAppearance.BorderColor;
                    newBtn.FlatAppearance.BorderSize = btnBoard.FlatAppearance.BorderSize;
                    newBtn.FlatStyle = btnBoard.FlatStyle;
                    
                    newBtn.Size = new System.Drawing.Size(btnBoard.Width, btnBoard.Height);
                    newBtn.Tag = new Square(i, j);
                    pnt = new Point(currentWidth, currentHeight);
                    newBtn.Location = pnt;
                    newBtn.BackgroundImageLayout = ImageLayout.Stretch;

                    newBtn.BackgroundImage = getImageBySign(board[z]);

                    newBtn.Click += lastlbl_Click;
                    newBtn.Cursor = Cursors.Hand;
                    newBtn.MouseDown += btnBoard_MouseDown;
                    newBtn.MouseUp += btnBoard_MouseUp;
                    newBtn.MouseLeave += btnBoard_MouseLeave;
                    newBtn.Click += btnBoard_Click;

                    Controls.Add(newBtn);

                    currentWidth += btnBoard.Width;
                    isColBlack = !isColBlack;
                    z++;
                }

                isRowBlack = !isRowBlack;
                currentHeight += btnBoard.Height;
            }
            Controls.Remove(btnBoard);
            this.ResumeLayout(false);
        }

        private void lastlbl_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (srcSquare != null)
            {
                // unselected
                if (matBoard[srcSquare.Row, srcSquare.Col] == b)
                {
                    matBoard[srcSquare.Row, srcSquare.Col].FlatAppearance.BorderColor = Color.RosyBrown;
                    srcSquare = null;
                }
                else
                {
                    dstSquare = (Square)b.Tag;
                    matBoard[dstSquare.Row, dstSquare.Col].FlatAppearance.BorderColor = Color.GreenYellow;

                    Thread t = new Thread(playMove);
                    t.Start();
                    //   t.IsBackground = true;
                    //playMove();
                }
            }
            else
            {
                if (!isGameOver)
                {
                    srcSquare = (Square)b.Tag;
                    matBoard[srcSquare.Row, srcSquare.Col].FlatAppearance.BorderColor = Color.GreenYellow;
                }
            }
        }

        // messages should be according the protocol.
        // index is the message number in the protocol
        private string[] messages =  {
            "Valid move",
            "Valid move - You check'd the other player!",
            "Game over - Checkmate!",
            "Invalid move - Not your piece!",
            "Invalid move - Destination is taken by one of your pieces",
            "Invalid move - Your king will be in check!",
            "Invalid move - Out of the bounds of the Board",
            "Invalid move - Illegal movement with piece",
            "Invalid move - Source and destination are the same",
            "Valid move - En Passant!",
            "Valid Move - You check'd the other player using En Passant!",
            "Game over - Checkmate using En Passant!",
            "Invalid move - Cannot castle in this position!",
            "Valid move - Castles!",
            "Valid move - You check'd the other player using Castles!",
            "Game over - Checkmate using Castles!",
            "Promotion",
            "Unknown message"
            };

        private string convertEngineToText(string m)
        {
            char res;
            bool b = char.TryParse(m, out res);
            int index = res - '0';

            if (!b || index < 0 || index >= messages.Length)
                return messages[messages.Length - 1];

            return messages[index];
        }

        private void playMove()
        {
            if (isGameOver)
                return;
            try
            {
                Invoke((MethodInvoker)delegate
                {
                    lblEngineCalc.Visible = true;

                    lblMove.Text = string.Format("Move from {0} to {1}", srcSquare, dstSquare);
                    lblMove.Visible = true;
                    //lblEngineCalc.Invalidate();

                    label2.Visible = false;
                    lblResult.Visible = false;

                    lblMove.Refresh();
                    label2.Refresh();
                    lblResult.Refresh();

                    // should send pipe to engine
                    enginePipe.sendEngineMove(srcSquare.ToString() + dstSquare.ToString());

                    // should get pipe from engine
                    string m = enginePipe.getEngineMessage();

                    if (!enginePipe.isConnected())
                    {
                        //MessageBox.Show("Connection to engine has lost. Bye bye.");
                        this.Close();
                        return;
                    }

                    string res = convertEngineToText(m);
                    char piece = ' ';
                    if (res == "Promotion")
                    {
                        PromotionChoice.Visible = true;
                        PromotionChoice.SelectedItem = "Queen";
                        InitiatePromotion.Visible = true;
                        this.Refresh();

                        waitForButton = true;
                        while (!this.promoteNow)
                        {
                            Application.DoEvents();
                        }
                        promoteNow = false;

                        piece = PromotionChoice.SelectedItem.ToString()[0];
                        enginePipe.sendEngineMove("" + piece);
                        res = convertEngineToText(enginePipe.getEngineMessage());
                        PromotionChoice.Visible = false;
                        InitiatePromotion.Visible = false;
                        this.Refresh();
                    }

                    switch(piece)
                    {
                        case 'B':
                            matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = (!isCurPlWhite) ? getImageBySign('b') : getImageBySign('B');
                            break;
                        case 'K':
                            matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = (!isCurPlWhite) ? getImageBySign('n') : getImageBySign('N');
                            break;
                        case 'R':
                            matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = (!isCurPlWhite) ? getImageBySign('r') : getImageBySign('R');
                            break;
                        case 'Q':
                            matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = (!isCurPlWhite) ? getImageBySign('q') : getImageBySign('Q');
                            break;
                    }

                    if (res.Contains("Castles"))
                    {
                        int rookCollumn = (dstSquare.Col > srcSquare.Col) ? 7 : 0;
                        int row = dstSquare.Row;
                        matBoard[row, (srcSquare.Col + dstSquare.Col) / 2].FlatAppearance.BorderColor = Color.RosyBrown;
                        matBoard[row, (srcSquare.Col + dstSquare.Col) / 2].BackgroundImage = matBoard[row, rookCollumn].BackgroundImage;
                        matBoard[row, rookCollumn].BackgroundImage = null;
                    }
                    else if (res.Contains("En Passant"))
                    {
                        matBoard[dstSquare.Row + (isCurPlWhite ? 1 : -1), dstSquare.Col].BackgroundImage = null;
                        matBoard[dstSquare.Row + (isCurPlWhite ? 1 : -1), dstSquare.Col].FlatAppearance.BorderColor = Color.RosyBrown;
                    }

                    
                    if (res.ToLower().StartsWith("valid") || res.ToLower().StartsWith("game over"))
                    {
                        playMovePiece();
                        isCurPlWhite = !isCurPlWhite;
                        lblCurrentPlayer.Text = res.ToLower().StartsWith("valid") ? (isCurPlWhite ? "White" : "Black") : "NaeNae Banaenae";

                        matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                        matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;

                        matBoard[srcSquare.Row, srcSquare.Col].FlatAppearance.BorderColor = Color.RosyBrown;
                        matBoard[dstSquare.Row, dstSquare.Col].FlatAppearance.BorderColor = Color.RosyBrown;
                        
                        if (res.ToLower().StartsWith("game over"))
                        {
                            isGameOver = true;
                            timer1.Stop();
                            timer2.Stop();
                        }
                        else
                        {
                            switchPlayers();
                        }
                    }

                    lblEngineCalc.Visible = false;
                    lblResult.Text = string.Format("{0}", res);
                    lblResult.Visible = true;
                    label2.Visible = true;
                    
                    lblEngineCalc.Refresh();
                    lblResult.Refresh();
                    label2.Refresh();

                });
            }
            catch
            {
            }
            finally
            {
                Invoke((MethodInvoker)delegate
                {
                    if (srcSquare != null)
                        matBoard[srcSquare.Row, srcSquare.Col].FlatAppearance.BorderColor = Color.RosyBrown;

                    if (dstSquare != null)
                        matBoard[dstSquare.Row, dstSquare.Col].FlatAppearance.BorderColor = Color.RosyBrown;

                    dstSquare = null;
                    srcSquare = null;
                });
            }
        }

        
        private void switchPlayers()
        {
            if (isCurPlWhite)
            {
                timer1.Start();
                timer2.Stop();
                player2Time += increment;
                textBox2.Text = getTimeRepresentation(player2Time);
            }
            else
            {
                timer2.Start();
                timer1.Stop();
                player1Time += increment;
                textBox1.Text = getTimeRepresentation(player1Time);
            }
        }

        private string getTimeRepresentation(double time)
        {
            return $"{(int)(time / 60):00}:{((int)time % 60):00}";
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (enginePipe != null)
            {
                enginePipe.sendEngineMove("quit");
                enginePipe.close();
            }
        }

        private void lblWaiting_Click(object sender, EventArgs e)
        {
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void InitiatePromotion_Click(object sender, EventArgs e)
        {
            promoteNow = true;
        }

        private void InitiateGame_Click(object sender, EventArgs e)
        {
            startGame = true;
        }

        private void playMovePiece()
        {
            
            wplayer.URL =  this.directoryBeginning + "\\move.mp3";
            wplayer.controls.play();
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            textBox1.Text = getTimeRepresentation(player1Time);
            player1Time -= 0.333d;

            if (player1Time <= 0)
            {
                isGameOver = true;
                lblCurrentPlayer.Text = "Black";
                lblResult.Text = "Black has won due to White running out of time!";
                timer1.Stop();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            textBox2.Text = getTimeRepresentation(player2Time);
            player2Time -= 0.333d;
                    
            if(player2Time <= 0)
            {
                isGameOver = true;
                lblCurrentPlayer.Text = "White";
                lblResult.Text = "White has won due to Black running out of time!";
                timer2.Stop();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.Refresh();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.Refresh();
        }

        private void lblResult_Click(object sender, EventArgs e)
        {

        }

        private void btnBoard_MouseDown(object sender, MouseEventArgs e)
        {
        }

        private void btnBoard_MouseUp(object sender, MouseEventArgs e)
        {
            ((Button)sender).Cursor = Cursors.Hand;
        }

        

        private void btnBoard_Click(object sender, EventArgs e)
        {
        }

        private void btnBoard_MouseLeave(object sender, EventArgs e)
        {
            ((Button)sender).Cursor = Cursors.Hand;
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void IncrementChoice_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        

        private void InitiateGame_MouseHover_1(object sender, EventArgs e)
        {
            this.InitiateGame.Image = global::chessGraphics.Properties.Resources.start_pressed;
            this.InitiateGame.Refresh();
        }

        private void InitiateGame_MouseLeave(object sender, EventArgs e)
        {
            this.InitiateGame.Image = global::chessGraphics.Properties.Resources.start_unpressed;
            this.InitiateGame.Refresh();
        }

        private void InitiatePromotion_MouseHover_1(object sender, EventArgs e)
        {
            this.InitiatePromotion.BackgroundImage = global::chessGraphics.Properties.Resources.promote_pressed;
            this.InitiatePromotion.Refresh();
        }

        private void InitiatePromotion_MouseLeave(object sender, EventArgs e)
        {
            this.InitiatePromotion.BackgroundImage = global::chessGraphics.Properties.Resources.promote_unpressed;
            this.InitiatePromotion.Refresh();
        }
    } 
}