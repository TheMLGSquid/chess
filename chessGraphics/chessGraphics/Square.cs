﻿using System;

namespace chessGraphics
{
    public class Square
    {
        private int _colIndex;
        private int _rowIndex;

        public Square(int row, int col)
        {
            _rowIndex = row;
            _colIndex = col;
        }

        public int Row
        {
            get { return _rowIndex; }
        }

        public int Col
        {
            get { return _colIndex; }
        }

        public override string ToString()
        {
            return Convert.ToChar('a' + _colIndex).ToString() + (8 - _rowIndex).ToString();
        }
    }
}